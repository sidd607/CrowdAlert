import React from 'react';
import App from './App';
import 'react-native';

import renderer from 'react-test-renderer';

// Tests if the all renders without errors
test('renders correctly', () => {
  const rendered = renderer.create(<App />).toJSON();
  expect(rendered).toBeTruthy();
});
